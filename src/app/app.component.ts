import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'tick-tack-toe';
  board: Array<string> = Array<string>(9).fill(null);
  history: Array<Array<string>> = new Array<Array<string>>();
  stepNumber = 0;
  xNext = false;
  winner = null;

  constructor() {
    this.history.push(this.board.slice());
  }

  squareClick(sq: number) {
    this.history = this.history.slice(0, this.stepNumber);
    if (this.board[sq]) {
      return;
    }
    this.board[sq] = this.xNext ? 'X' : 'O';
    this.history.push(this.board.slice());
    this.xNext = !this.xNext;
    this.stepNumber++;
    this.winner = this.calculateWinner(this.board);
  }

  jumpTo(move: number) {
    if (this.winner && (move !== this.stepNumber)) {
      this.winner = null;
    }
    this.stepNumber = move;
    this.board = this.history[move].slice();
    this.xNext = move % 2 > 0 ? true : false;
  }

  status(): string {
    if (this.winner) {
      return 'Winner: ' + this.winner;
    } else {
      return 'Next player: ' + (this.xNext ? 'X' : 'O');
    }
  }

  calculateWinner(squares: Array<string>): string {
    const lines = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6]
    ];
    for (let i = 0; i < lines.length; i++) {
      const [a, b, c] = lines[i];
      if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
        return squares[a];
      }
    }
    return null;
  }
}
